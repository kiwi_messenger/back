FROM node:20-bookworm
RUN npm install -g npm

USER node
COPY --chown=1000:1000 ./package.json /application/package.json
COPY --chown=1000:1000 ./package-lock.json /application/package-lock.json

WORKDIR /application/

RUN npm install --omit=dev

COPY --chown=1000:1000 ./src/ /application/src

CMD ["npm","run","start"]
