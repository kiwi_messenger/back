import { describe, it } from 'node:test';
import assert from 'node:assert/strict';

import { set, get, del } from "../../src/service/saveMessage.js";

const { randomBytes } = await import("node:crypto");


describe('service/saveMessage', () => {
  // it('1 message 1Go to 2Go', async() => {
  //   // This test passes because it does not throw an exception.
  //   const id = randomBytes(33).toString("base64url")
  //   const datas = []
  //   for (let index = 0; index < 1; index+=1) {
  //     const data = randomBytes(Math.random() * 250000000 + 250000000);
  //     await set(id,{data,test:"test"});
  //     datas.push(data)
  //   }
  //   const dataGet = await get(id);
  //   // assert.strictEqual(1,dataGet.length);
  //   // assert.strictEqual(1,datas.length);

  //   // for (let index = 0; index < 1; index+=1) {
  //   //   assert.strictEqual(datas[index].length,dataGet[index].data.length);
  //   //   assert.strictEqual("test",dataGet[index].test);
  //   // }

    
  //   // dataGet = await get(id);
  //   // assert.strictEqual(0,dataGet.length);
  //   await del(id);
  // });

  it('2 message 10Mo to 50Mo', async() => {
    // This test passes because it does not throw an exception.
    const id = randomBytes(33).toString("base64url")
    const datas = []
    for (let index = 0; index < 2; index+=1) {
      const data = randomBytes(Math.random() * 40000000 + 10000000);
      // eslint-disable-next-line no-await-in-loop
      await set(id,{data,test:"test"});
      datas.push(data)
    }
    let dataGet = await get(id);
    assert.strictEqual(2,dataGet.length);
    assert.strictEqual(2,datas.length);

    for (let index = 0; index < 2; index+=1) {
      assert.strictEqual(0,dataGet[index].data.compare(datas[index]));
      assert.strictEqual("test",dataGet[index].test);
    }

    
    dataGet = await get(id);
    assert.strictEqual(0,dataGet.length);
    await del(id);
  });

  it('100 message 1Mo to 5Mo', async() => {
      // This test passes because it does not throw an exception.
      const id = randomBytes(33).toString("base64url")
      const datas = []
      for (let index = 0; index < 100; index+=1) {
        const data = randomBytes(Math.random() * 4000000 + 1000000);
        // eslint-disable-next-line no-await-in-loop
        await set(id,{data,test:"test"});
        datas.push(data)
      }
      let dataGet = await get(id);
      assert.strictEqual(100,dataGet.length);
      assert.strictEqual(100,datas.length);

      for (let index = 0; index < 100; index+=1) {
        assert.strictEqual(0,dataGet[index].data.compare(datas[index]));
        assert.strictEqual("test",dataGet[index].test);
      }
  
      
      dataGet = await get(id);
      assert.strictEqual(0,dataGet.length);
      await del(id);
    });

  it('1000 message 25ko to 50Ko', async() => {
    // This test passes because it does not throw an exception.
    const id = randomBytes(33).toString("base64url")
    const datas = []
    for (let index = 0; index < 1000; index+=1) {
      const data = randomBytes(Math.random() * 50000 + 25000);
      // eslint-disable-next-line no-await-in-loop
      await set(id,{data,test:"test"});
      datas.push(data)
    }
    let dataGet = await get(id);
    assert.strictEqual(1000,dataGet.length);
    assert.strictEqual(1000,datas.length);
    for (let index = 0; index < 1000; index+=1) {
      assert.strictEqual(0,dataGet[index].data.compare(datas[index]));
      assert.strictEqual("test",dataGet[index].test);
    }

    
    dataGet = await get(id);
    assert.strictEqual(0,dataGet.length);
    await del(id);
  });

  it('10000 message 256o to 512o', async() => {
    // This test passes because it does not throw an exception.
    const id = randomBytes(33).toString("base64url")
    const datas = []
    for (let index = 0; index < 10000; index+=1) {
      const data = randomBytes(Math.random() * 512 + 256);
      // eslint-disable-next-line no-await-in-loop
      await set(id,{data,test:"test"});
      datas.push(data)
    }
    let dataGet = await get(id);
    assert.strictEqual(10000,dataGet.length);
    assert.strictEqual(10000,datas.length);
    for (let index = 0; index < 10000; index+=1) {
      assert.strictEqual(0,dataGet[index].data.compare(datas[index]));
      assert.strictEqual("test",dataGet[index].test);
    }

    
    dataGet = await get(id);
    assert.strictEqual(0,dataGet.length);
    await del(id);
  });

});

