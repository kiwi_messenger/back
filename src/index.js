import fs from "fs";
import path from "path";
import url from "url";

const dirname = path.dirname(url.fileURLToPath(import.meta.url));

fs.readdirSync(path.join(dirname, "eventlisteners")).forEach((file) => {
  import(`./eventlisteners/${file}`);
});
