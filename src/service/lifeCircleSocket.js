import EventEmitter from "events";
import io from "./socket.io.js";

const lifeCircleSocket = new EventEmitter();
export default lifeCircleSocket;

io.on("connection", (socket) => {
  // socket.onAny((...args) => {
  //   console.log(
  //     socket.id,
  //     {
  //       id: socket?.user?.id,
  //       pseudo: socket?.user?.pseudo,
  //     },
  //     args,
  //   );
  // });
  socket.emitP = async (...args) =>
    new Promise((resolve) => {
      args[args.length] = (data) => {
        resolve(data);
      };

      socket.emit(...args);
    });

  lifeCircleSocket.emit("connection", socket);
});
