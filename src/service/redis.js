import redis from "redis";

const host = "redis";
const password = "root";

const makeClient = async (index) => {
  const client = redis.createClient({
    socket: { host },
    password,
    database: index,
  });
  await client.connect();
  return client;
};

export const sockets = await makeClient(0);
export const devices = await makeClient(1);
export const pseudos = await makeClient(2);
export const accounts = await makeClient(3);
