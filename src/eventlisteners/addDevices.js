import { encode } from "notepack.io";

import { devices, pseudos, accounts } from "../service/redis.js";

import lifeCircleSocket from "../service/lifeCircleSocket.js";

lifeCircleSocket.on("auth", (socket) => {
  socket.on("addDevices", async (devicesFromClient, callback) => {
    const pseudoLowerCase = socket.user.pseudo.toLowerCase();
    await Promise.all(
      devicesFromClient.map((device) =>
        Promise.all([
          pseudos.sAdd(pseudoLowerCase, device.id),
          accounts.sAdd(socket.user.idAccountBin, device.id),
          devices.set(
            device.id,
            encode({
              pseudo: socket.user.pseudo,
              key: device.key,
              idAccount: socket.user.idAccountBin,
            }),
            "NX",
          ),
        ]),
      ),
    );
    callback(200);
    console.info(`${socket.user.pseudo}(${socket.user.id}) addDevices 200`);
  });
});
