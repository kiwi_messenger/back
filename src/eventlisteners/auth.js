import { sockets } from "../service/redis.js";
import { get } from "../service/saveMessage.js";

import lifeCircleSocket from "../service/lifeCircleSocket.js";
import roomAuth from "../service/roomAuth.js";
import { connecters, socketId } from "../service/store.js";

lifeCircleSocket.on("connection", (socket) => {
  socket.on("auth", async (token, callback) => {
    if (token.compare(socket.user.authToken) === 0) {
      if (connecters[socket.user.id]) {
        console.info(`${socket.user.pseudo}(${socket.user.id}) auth 409`);
        callback(409);
        return;
      }
      sockets.set(socket.user.id, socket.id);
      const connecter = {
        pseudo: socket.user.pseudo,
        id: socket.user.id,
        idAccount: socket.user.idAccount,
      };
      roomAuth.emit("addConnecter", connecter);
      socket.join("/auth");
      lifeCircleSocket.emit("auth", socket);
      callback(200, { connecters, messages: await get(socket.user.id) });
      console.info(`${socket.user.pseudo}(${socket.user.id}) auth 200`);
      connecters[socket.user.id] = connecter;
      socketId[socket.user.id] = socket.id;
    } else {
      callback(401);
    }
  });
});
