import { decode } from "notepack.io";
import { commandOptions } from "redis";

import { devices, pseudos, accounts } from "../service/redis.js";
import { del } from "../service/saveMessage.js";

import lifeCircleSocket from "../service/lifeCircleSocket.js";

lifeCircleSocket.on("auth", (socket) => {
  socket.on("delDevice", async (idBin, callback) => {
    let data = await devices.get(
      commandOptions({ returnBuffers: true }),
      idBin,
    );
    if (!data) {
      console.info(`${socket.user.pseudo}(${socket.user.idBin}) delDevice 404`);
      callback(404);
      return;
    }
    data = decode(data);
    if (data.idAccount.equals(socket.user.idAccountBin)) {
      await Promise.all([
        devices.del(idBin),
        pseudos.sRem(socket.user.pseudo.toLowerCase(), idBin),
        accounts.sRem(socket.user.idAccountBin, idBin),
        del(idBin.toString("base64url")),
      ]);
      callback(200);
      console.info(`${socket.user.pseudo}(${socket.user.id}) delDevice 200`);
    } else {
      callback(401);
      console.info(`${socket.user.pseudo}(${socket.user.id}) delDevice 401`);
    }
  });
});
