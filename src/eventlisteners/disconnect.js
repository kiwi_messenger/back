import { sockets } from "../service/redis.js";
import lifeCircleSocket from "../service/lifeCircleSocket.js";
import roomAuth from "../service/roomAuth.js";
import { connecters, socketId } from "../service/store.js";

lifeCircleSocket.on("auth", (socket) => {
  socket.on("disconnect", () => {
    if (socket.user) {
      delete connecters[socket.user.id];
      delete socketId[socket.user.id];
      roomAuth.emit("delConnecter", socket.user.id);
      sockets.del(socket.user.id);
      console.info(`${socket.user.pseudo}(${socket.user.id}) est disconnect`);
    }
  });
});

lifeCircleSocket.on("unauth", (socket) => {
  socket.removeAllListeners("disconnect");
});
